var app = angular.module("parkingApp", []);

app.controller('ParkingCtrl', ["$scope", "ParkingService", function ($scope, ParkingService) {
	$scope.places = angular.fromJson(localStorage.getItem("parking")) || [];
	
	$scope.regularQty = 15;
	$scope.truckQty = 10;
	$scope.disabledQty = 5;

	$scope.generatePlaces = function () {
		
		ParkingService.addPlaces("regular", $scope.regularQty);
		ParkingService.addPlaces("truck", $scope.truckQty);
		ParkingService.addPlaces("disabled", $scope.disabledQty);

		$scope.places = ParkingService.places;
		$scope.$emit("saveState");
	}
	
	$scope.$on("saveState", function () {
		localStorage.setItem("parking", angular.toJson($scope.places));
	});

	$scope.$on("addPlace", function (place) {
		$scope.places.push(place);
	})

}]);

app
.controller("PlaceCtrl", function ($scope, Place, Car) {
	$scope.createPlace = function (type) {
		return new Place(type);
	}
	$scope.removeCar = function (place) {
		delete place.car;
		$scope.$emit("saveState");
	};
	$scope.parkCar = function (place, carType) {
		if (!place.car) {
			place.car = new Car(carType);
			$scope.$emit("saveState");
		}
	}
	$scope.showParkOption = function (placeType, carType) {
		if (placeType === "regular") {
			if (carType === "truck") {
				return false;
			} else {
				return true;
			}
		}
		if (placeType === "truck") {
			return true;
		}
		if (placeType === "disabled") {
			if (carType === "disabled") {
				return true;
			} else {
				return false;
			}
		}
	}
	$scope.$on("createPlaces", function (data) {
		for (var i = 0; i<data.qty; i++) {
			$scope.$emit("addPlace", $scope.createPlace(data.type));
		}
	})
})
.directive("parkingPlace", function () {
	return {
		templateUrl: "templates/place.html",
		restrict: "E"
	}
});

app.service("ParkingService", function (Place) {
	var places = [];
	var addPlaces = function (type, qty) {
		for (var i=0; i<qty; i++) {
			places.push(new Place(type));
		}
	}
	return {
		places: places,
		addPlaces: addPlaces
	}
});
app.factory('Place', function () {
	return function (type) {
		return {
			type: type
		}
	}
});
app.factory('Car', function () {
	return function (type) {
		return {
			type: type
		}
	}
});